#!/usr/bin/env groovy

def buildArtifact(){
    sh "mvn package"
}
return this

def buildImage(){
    withCredentials([
            usernamePassword(credentialsId: "dockerhub-credentials", usernameVariable: "USER", passwordVariable: "PASS")
    ]){
        sh "docker build -t my-app:1.6.0 ."
        sh "docker tag my-app:1.0 golebu2023/image-registry:my-app-1.6.0"
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push golebu2023/image-registry:my-app-1.6.0"
    }
}
return this